# SMS Development Cartridge

A homemade 512KB Sega Master System Development Cartridge. It's made from an
*After Burner* cartridge based on `Sega 315-5235` paging chip. The trick is to
replace the `Sega MPR` chip by an `AMIC A29040B` flash memory.

The flash memory chip can be programmed and read by an `Arduino Mega 1280`.

The `AVR Atmega 1280` embedded in the `Arduino Mega 1280` is programmed in
*Mara*.

![SMS][5]

![SMS][6]

## Hardware

### Development cartridge

![Cartridge][7]

#### Sega MPR (4-Mega) pinout


                ----_----
              -|   \_/   |- Vcc
          A16 -|         |- A18
          A15 -|         |- A17
          A12 -|         |- A14
          A07 -|         |- A13
          A06 -|         |- A08
          A05 -|  SEGA   |- A09
          A04 -|         |- A11
          A03 -|    MPR  |- A15
          A02 -|         |- A10
          A01 -|         |- ~CE
          A00 -|         |- D07
          D00 -|         |- D06
          D01 -|         |- D05
          D02 -|         |- D04
          GND -|         |- D03
                ---------

#### Sega 315-5235 Paging chip pinout

                -----_-----
          A07 -|    \_/    |- A08
          A06 -|           |- A09
          A05 -|           |- A10
          A04 -|           |- A11
          A03 -|           |- A12
          A02 -|           |- A13
          A01 -|           |- A14   IN
          A00 -|           |- A15   IN
    OUT   A14 -|           |- ~RD   IN
    OUT   A15 -|  SEGA     |- ~CE   IN
          GND -|           |- Vcc
    OUT   A16 -| 315-5235  |- ~WR   IN
    OUT   A17 -|           |- SRAM ~CE
    OUT   A18 -|           |- SRAM ~WE
    OUT  ~CE0 -|           |-
    OUT  ~CE1 -|           |- GND
    OUT  ~CE2 -|           |- ~RESET
          D00 -|           |- GND
          D01 -|           |- SRAM ~OE
          D02 -|           |- D07
          D03 -|           |- D04
                -----------

#### SMS Cartridge slot pinout


    Back       5V .. ~WR         Front
              Meq .. ~ROM/~RAM
            ~M8-B .. A14
              A13 .. A08
              A09 .. A11
            ~M0-7 .. A10
           ~CrtOE .. D07
              D06 .. D05
              D04 .. D03
              GND .. GND
              GND .. D02
              D01 .. D00
              A00 .. A01
              A02 .. A03
              A04 .. A05
              A06 .. A07
              A12 ..
               5V .. A15
              ~M1 .. I/Oreq
         ~Refresh .. Halt
            ~Wait .. Int
             JyDs .. ~BusReq
          ~BusAck .. ~Reset
           ~Clock .. Jread
            ~MC-F .. ~NMI

![Cartridge and flash memories][9]

### Homemade flash memory programmer

![Homemade flash memory programmer][11]

#### A29040B to Arduino Mega 1280

                ----_----
    19  - A18 -|   \_/   |- VCC - 5V
    21  - A16 -|         |- ~WR - 51
    42  - A15 -|         |- A17 - 20
    45  - A12 -|         |- A14 - 43
    30  - A07 -|         |- A13 - 44
    31  - A06 -|         |- A08 - 49
    32  - A05 -| A29040B |- A09 - 48
    33  - A04 -|         |- A11 - 46
    34  - A03 -|         |- ~OE - 52
    35  - A02 -|         |- A10 - 47
    36  - A01 -|         |- ~CE - 53
    37  - A00 -|         |- D07 - 29
    22  - D00 -|         |- D06 - 28
    23  - D01 -|         |- D05 - 27
    24  - D02 -|         |- D04 - 26
    GND - GND -|         |- D03 - 25
    	        ---------

### Upload the program to Arduino Mega 1280

    $ make dump DEV=/dev/ttyU0

## Example

    $ ./smsdev.py -d /dev/ttyU0 write -i ./Sonic_the_Hedgehog_2.sms
    *** SMS Development Cartridge ***
    Pierre Surply 2013
    www.psurply.com
    Connecting to programmer (/dev/ttyU0)...    Done
    Checking header...                          Done
    Checksum            995F
    Product code        14421
    Version             0
    Region              SMS Export
    Rom size            512KB
    Erasing cartridge data...			Done
    Writing cartridge data from ./Sonic_the_Hedgehog_2.sms
    100% (524288 B / 524288 B)
    Finished !

![Sonic 2][10]

## Dependencies

### Cartridge

* [Mara][1] (>= 13)
* [Avrdude][2]

### Driver

* [Python 2.7][3]
* [pySerial][4]

## Contact

* Pierre Surply (pierre.surply@gmail.com)

[1]: http://mara.psurply.com/
[2]: http://savannah.nongnu.org/projects/avrdude
[3]: http://www.python.org/
[4]: http://pyserial.sourceforge.net/
[5]: http://www.psurply.com/sms/sms01.jpg
[6]: http://www.psurply.com/sms/sms02.jpg
[7]: http://www.psurply.com/sms/sms03.jpg
[9]: http://www.psurply.com/sms/sms05.jpg
[10]: http://www.psurply.com/sms/sms06.jpg
[11]: http://www.psurply.com/sms/sms07.jpg
