#!/usr/bin/python2
## smsdev.py for SMSdev
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Sat May 11 16:31:39 2013 Pierre Surply
## Last update Fri Aug 16 15:04:15 2013 Pierre Surply
##

import serial
import time
import argparse
import sys
import os

def check_header(com):
    if com.read() != "*":
        sys.stderr.write("Bad header\n")
        exit(2)

def parse_args():
    parser = argparse.ArgumentParser(description= \
                                    "Driver for a homemade 32KB Sega Master System Development Cartridge")
    parser.add_argument('order',                        \
                            choices=['read', 'write', 'erase', 'check-header'],  \
                            help="""
dump : dump the data contained in the cartridge to the output file (specified with -o option)
""")
    parser.add_argument('-d', '--device',               \
                            metavar='DEV',              \
                            type=str,                   \
                            nargs=1,                    \
                            default=["/dev/ttyUSB0"],   \
                            help="set device (default: %(default)s)")
    parser.add_argument('-o',                           \
                            metavar='OUTPUT',           \
                            type=str,                   \
                            nargs=1,                    \
                            default=["out.sms"],        \
                            help="set output file (default: %(default)s)")
    parser.add_argument('-i',                           \
                            metavar='INPUT',            \
                            type=str,                   \
                            nargs=1,                    \
                            default=["in.sms"],         \
                            help="set output file (default: %(default)s)")
    parser.add_argument('-s',                           \
                            metavar='SIZE',             \
                            type=int,                   \
                            nargs=1,                    \
                            default=[32],               \
                            help="set size (default: %(default)s)")

    return vars(parser.parse_args())


def connect_mcu(device):
    sys.stdout.write("Connecting to programmer ({0})...".format(device))
    try:
        com = serial.Serial(device, 115200)
        sys.stdout.write("\tDone\n")
    except serial.serialutil.SerialException:
        sys.stdout.write("\tFailure !\n")
        exit(2)
    time.sleep(2)
    return com

def progress(f, n):
    for x in range(n):
        sys.stdout.write("\r{0}% ({1} B / {2} B)".format(((x + 1) * 100) /      \
                                                             n,                 \
                                                             (x + 1),           \
                                                             n))
        sys.stdout.flush()
        f(x)
    sys.stdout.write("\n")

def erase_flash(com):
    com.write('e')
    check_header(com)
    sys.stdout.write("Erasing cartridge data...")
    sys.stdout.flush()
    check_header(com)
    sys.stdout.write("\t\t\tDone\n")

def read_flash(com, path, size):
    f = open(path, "wb")
    com.write('r')
    check_header(com)
    print("Reading cartridge data to {0}...".format(path))
    progress(lambda x : f.write(com.read(1)), size * 1024)
    f.close()

def write_flash(com, path, header):
    size = os.path.getsize(path)
    if size > (512 * 1024):
        sys.stderr.write("File must be < 512kB\n")
        exit(2)
    f = open(path, "rb")
    com.write('w')
    check_header(com)
    time.sleep(2)
    sys.stdout.write("Writing cartridge data from {0}...\n".format(path))
    progress(lambda x : com.write(f.read(1)), size)
    f.close()

def check_header_cart(path):
    f = open(path, "rb")
    sys.stdout.write("Checking header...")
    sys.stdout.flush()
    f.seek(0x7ff0)
    header = [ord(x) for x in list(f.read(16))]
    if "".join(chr(x) for x in header[:8]) == "TMR SEGA":
        sys.stdout.write("\t\t\t\tDone\n")
        sys.stdout.write("Checksum\t{0:X}\n".format((header[10] << 8) | \
                                                           header[11]))
        sys.stdout.write("Product code\t{0}\n".format(header[12] +           \
                                                           header[13] * 100 +   \
                                                           (header[14] >> 4) * 10000))
        sys.stdout.write("Version\t\t{0}\n".format(header[14] & 0xF))
        sys.stdout.write("Region\t\t")
        region_code = header[15] >> 4
        if region_code == 0x3:
            sys.stdout.write("SMS Japan\n")
        elif region_code == 0x4:
            sys.stdout.write("SMS Export\n")
        elif region_code == 0x5:
            sys.stdout.write("GG Japan\n")
        elif region_code == 0x6:
            sys.stdout.write("GG Export\n")
        elif region_code == 0xF:
            sys.stdout.write("GG International\n")
        else:
            sys.stderr.write("Bad region code\n")
        rom_size = header[15] & 0xF
        if rom_size == 0xa:
            size = 8
        elif rom_size == 0xb:
            size = 16
        elif rom_size == 0xc:
            size = 32
        elif rom_size == 0xd:
            size = 48
        elif rom_size == 0xe:
            size = 64
        elif rom_size == 0xf:
            size = 128
        elif rom_size == 0x0:
            size = 256
        elif rom_size == 0x1:
            size = 512
        else:
            sys.stderr.write("Bad rom size\n")
        sys.stdout.write("Rom size\t{0}KB\n".format(size))
    else:
        sys.stderr.write("\t\t\t\tFailure\n")
        exit(2)
    f.close()
    return header

def main():
    sys.stdout.write("""*** SMS Development Cartridge ***
Pierre Surply 2013
www.psurply.com\n""")
    args = parse_args()
    com = connect_mcu(args['device'][0])
    header = []
    if args["order"] in ["check-header"]:
        header = check_header_cart(args['i'][0])
    if args["order"] in ["erase", "write"]:
        erase_flash(com)
    if args["order"] == "read":
        read_flash(com, args['o'][0], args['s'][0])
    elif args["order"] == "write":
        write_flash(com, args['i'][0], header)
    sys.stdout.write("Finished !\n")

if __name__ == "__main__":
    main()
