##
## Makefile
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Sun Jan 20 12:24:46 2013 Pierre Surply
## Last update Sun Aug 25 13:14:15 2013 Pierre Surply
##

MCU		= 1280
DEV		?= /dev/ttyU0

MARAC		= marac
MFLAGS		= -mmcu atmega$(MCU)

AVRDUDE		= avrdude
PARTNO		= m$(MCU)
PROG		= arduino -b 57600
AFLAGS		= -p $(PARTNO) -c $(PROG) -P $(DEV) -e

MAIN		= devcart
MAINHEX		= $(MAIN).hex

SRC		= $(wildcard *.mr)

all: $(MAINHEX)

$(MAINHEX): $(SRC)
	$(MARAC) $(MFLAGS) $(MAIN)

flash: $(MAINHEX)
	$(AVRDUDE) $(AFLAGS) -U "flash:w:$<"

clean::
	rm -f *~ *.hex
	$(MARAC) -clean
